# Game Engine in WebGL

## About
A game engine built in WebGL.

For testing purposes, I created a game where you shoot monsters using a sniper rifle.

![Local Image](images/game1.jpg)

![Local Image](images/game2.jpg)
