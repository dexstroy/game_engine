/*
Razred model

Predstavlja en model, katerega uporablja razred gameObject
Razred model pokrije vse osnovne funkcije izrisa modela
*/
class JSONmodel{
	constructor(path, texturePath, clamp){
		// pot do datoteke kjer se nahajajo podatki
		this.path = path;
		// pot do slike ki je tekstura
		this.texturePath = texturePath;

		// Buffer za tocke
		this.vertexPositionBuffer = null;
		// Buffer za koordinate textur
		this.textureCoordBuffer = null;
		// pointer za tocke
		this.indiceBuffer = null;

		// Tu je shranjena tekstura za gameObject
		this.texture;

		this.textureReady = false;
		this.modelReady = false;

		this.vertexCount = 0;

		this.jsonFile;

		this.objIndices;

		// preveri ali je bil clamp sploh definiran
		if(typeof clamp === "undefined") this.clamp = false;
		else this.clamp  = clamp;

		// prebere file z koordinatami
		this.readFile();
		// prebere sliko texture
		this.readTexture();
	}


	// normalizira tabelo števil na range med maxNew in minNew
	normalizeArray(array, maxNew = 1, minNew = 0){
	    let normalized = [];
	    let max = Math.max(...array);
	    let min = Math.min(...array);
	    let c = (maxNew - minNew)/(max - min);

	    for(let i = 0; i < array.length; i++){
	        normalized.push(c * ((array[i]-max)+max));
	    }
	    return normalized;
	}

	loadModel(text){

		// pretvorim v json
		this.jsonFile = JSON.parse(text);
		console.log(this.jsonFile);

		var objVertices = this.normalizeArray(this.jsonFile.meshes[0].vertices, 1, -1);
		this.objIndices = [].concat.apply([], this.jsonFile.meshes[0].faces);
		var objTexCoords = this.jsonFile.meshes[0].texturecoords[0];
		var bananaNormals = this.jsonFile.meshes[0].normals;

		this.vertexPositionBuffer = gl.createBuffer();
		gl.bindBuffer(gl.ARRAY_BUFFER, this.vertexPositionBuffer);
		gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(objVertices), gl.STATIC_DRAW);

		this.textureCoordBuffer = gl.createBuffer();
		gl.bindBuffer(gl.ARRAY_BUFFER, this.textureCoordBuffer);
		gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(objTexCoords), gl.STATIC_DRAW);

		this.indiceBuffer = gl.createBuffer();
		gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.indiceBuffer);
		gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(this.objIndices), gl.STATIC_DRAW);

    	console.log("Model prebran");
    	this.modelReady = true;
    	console.log(this.jsonFile);


	}

	// prebere datoteko in nastavi event ko se model nalozi da klice funkcijo loadModel
	readFile(){
		var request = new XMLHttpRequest();
		request.open("GET", this.path);
		var self = this;
		request.onreadystatechange = function () {
			if (request.readyState == 4) {
				self.loadModel(request.responseText);
			}
		}
		request.send();
	}



	readTexture() {

  		this.texture = gl.createTexture();
  		this.texture.image = new Image();
  		var self = this;
		this.texture.image.onload = function () {
	    	self.handleTextureLoaded();
  		}	
  		this.texture.image.src = this.texturePath;
	}


	handleTextureLoaded() {
		gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);

		// Third texture usus Linear interpolation approximation with nearest Mipmap selection
		gl.bindTexture(gl.TEXTURE_2D, this.texture);
		// preveri ali lahko uporabi clamp metodo
		if(this.clamp){
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
        	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
        	console.log("Model clamp uporabljen");
		}
		gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, this.texture.image);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
		gl.generateMipmap(gl.TEXTURE_2D);

		gl.bindTexture(gl.TEXTURE_2D, null);

		this.textureReady = true;

		
	}


	// izrise geometrijo
	draw(){
		// preveri ali ze lahko naredi izris
		if(!this.textureReady || !this.modelReady) return;
		//console.log("Izris")
		// aktiviram texturo
		gl.activeTexture(gl.TEXTURE0);
		gl.bindTexture(gl.TEXTURE_2D, this.texture);
  		gl.uniform1i(shaderProgram.samplerUniform, 0);

		gl.bindBuffer(gl.ARRAY_BUFFER, this.vertexPositionBuffer);
		gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, 3, gl.FLOAT, gl.FALSE, 3 * Float32Array.BYTES_PER_ELEMENT, 0);
		// gl.enableVertexAttribArray(shaderProgram.vertexPositionAttribute);

		gl.bindBuffer(gl.ARRAY_BUFFER, this.textureCoordBuffer);
		gl.vertexAttribPointer(shaderProgram.textureCoordAttribute, 2, gl.FLOAT, gl.FALSE, 2 * Float32Array.BYTES_PER_ELEMENT, 0);
		// gl.enableVertexAttribArray(shaderProgram.textureCoordAttribute);

		gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, this.indiceBuffer);
		setMatrixUniforms();
		gl.drawElements(gl.TRIANGLES, this.objIndices.length, gl.UNSIGNED_SHORT, 0);
		
	}
	
}
