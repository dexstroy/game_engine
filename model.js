
class model{
	constructor(path, texturePath, clamp){
		// pot do datoteke kjer se nahajajo podatki
		this.path = path;
		// pot do slike ki je tekstura
		this.texturePath = texturePath;

		// Buffer za tocke
		this.vertexPositionBuffer = null;
		// Buffer za koordinate textur
		this.textureCoordBuffer = null;
		// pointer za tocke
		this.indiceBuffer = null;

		// Tu je shranjena tekstura za gameObject
		this.texture;

		this.textureReady = false;
		this.modelReady = false;

		this.vertexCount = 0;

		this.jsonFile;

		this.objIndices;


		// preveri ali je bil clamp sploh definiran
		if(typeof clamp === "undefined") this.clamp = false;
		else this.clamp  = clamp;

		// prebere file z koordinatami
		this.readFile();
		// prebere sliko texture
		this.readTexture();
	}


	// prebere datoteko in nastavi event ko se model nalozi da klice funkcijo loadModel
	readFile(){
		var request = new XMLHttpRequest();
		request.open("GET", this.path);
		var self = this;
		request.onreadystatechange = function () {
			if (request.readyState == 4) {
				self.loadModel(request.responseText);
			}
		}
		request.send();
	}

	// ko dobi podatke klice to fukcijo kjer napolni bufferje
	loadModel(data){
		var lines = data.split("\n");
		var vertexCount = 0;
		var vertexPositions = [];
		var vertexTextureCoords = [];

		for (var i in lines) {
		var vals = lines[i].replace(/^\s+/, "").split(/\s+/);
			if (vals.length == 5 && vals[0] != "//") {
				// It is a line describing a vertex; get X, Y and Z first
				vertexPositions.push(parseFloat(vals[0]));
				vertexPositions.push(parseFloat(vals[1]));
				vertexPositions.push(parseFloat(vals[2]));

				// And then the texture coords
				vertexTextureCoords.push(parseFloat(vals[3]));
				vertexTextureCoords.push(parseFloat(vals[4]));

				vertexCount += 1;
				}
		}

		this.vertexPositionBuffer = gl.createBuffer();
		gl.bindBuffer(gl.ARRAY_BUFFER, this.vertexPositionBuffer);
		gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertexPositions), gl.STATIC_DRAW);
		this.vertexPositionBuffer.itemSize = 3;
		this.vertexPositionBuffer.numItems = vertexCount;

		this.textureCoordBuffer = gl.createBuffer();
		gl.bindBuffer(gl.ARRAY_BUFFER, this.textureCoordBuffer);
		gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertexTextureCoords), gl.STATIC_DRAW);
		this.textureCoordBuffer.itemSize = 2;
		this.textureCoordBuffer.numItems = vertexCount;

		this.modelReady = true;

		console.log("Bufferji uspesno nalozeni");
	}


	// prebere texturo
	readTexture() {

  		this.texture = gl.createTexture();
  		this.texture.image = new Image();
  		var self = this;
		this.texture.image.onload = function () {
	    	self.handleTextureLoaded();
  		}	
  		this.texture.image.src = this.texturePath;
	}


	handleTextureLoaded() {
		gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);

		// Third texture usus Linear interpolation approximation with nearest Mipmap selection
		gl.bindTexture(gl.TEXTURE_2D, this.texture);

		// preveri ali lahko uporabi clamp metodo
		if(this.clamp){
			gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
        	gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
        	console.log("Model clamp uporabljen");
		}
		gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, this.texture.image);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
		gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
		gl.generateMipmap(gl.TEXTURE_2D);

		gl.bindTexture(gl.TEXTURE_2D, null);

		this.textureReady = true;

		
	}

	// izris geometrije
	draw(){
		// preveri ali ze lahko naredi izris
		if(!this.textureReady || !this.modelReady) return;

		// aktiviram texturo
		gl.activeTexture(gl.TEXTURE0);
		gl.bindTexture(gl.TEXTURE_2D, this.texture);
  		gl.uniform1i(shaderProgram.samplerUniform, 0);
  		
		// Set the texture coordinates attribute for the vertices.
		gl.bindBuffer(gl.ARRAY_BUFFER, this.textureCoordBuffer);
		gl.vertexAttribPointer(shaderProgram.textureCoordAttribute, this.textureCoordBuffer.itemSize, gl.FLOAT, false, 0, 0);

		// Draw the world by binding the array buffer to the world's vertices
		// array, setting attributes, and pushing it to GL.
		gl.bindBuffer(gl.ARRAY_BUFFER, this.vertexPositionBuffer);
		gl.vertexAttribPointer(shaderProgram.vertexPositionAttribute, this.vertexPositionBuffer.itemSize, gl.FLOAT, false, 0, 0);

		// Draw the cube.
		setMatrixUniforms();
		gl.drawArrays(gl.TRIANGLES, 0, this.vertexPositionBuffer.numItems);
	}
}