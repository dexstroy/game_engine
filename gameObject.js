/*
lahko sprejme 2 tipa razredov kot model:
	model
	JSONmodel

Razred predstavlja en objekt kateremu lahko spreminjam pozicijo, velikost...
*/


class gameObject{
	
	constructor(model, position){
		this.model = model;

		this.position = position;
		this.scale = [1,1,1];

		this.Xrotation = 0;
		this.Yrotation = 0;
		this.Zrotation = 0;

	}

	degToRad(degrees) {
		return degrees * Math.PI / 180;
	}


	draw(){
		
		mvPushMatrix();
		mat4.translate(mvMatrix, this.position);
		mat4.rotate(mvMatrix, this.degToRad(this.Yrotation), [0,1,0]);
		mat4.rotate(mvMatrix, this.degToRad(this.Xrotation), [1,0,0]);
		mat4.rotate(mvMatrix, this.degToRad(this.Zrotation), [0,0,1]);

		mat4.scale(mvMatrix, this.scale);
		this.model.draw();
		mvPopMatrix()
		
	}
}