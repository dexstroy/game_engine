// Global variable definitionvar canvas;
var canvas;
var gl;
var shaderProgram;

// Buffers
var worldVertexPositionBuffer = null;
var worldVertexTextureCoordBuffer = null;

var grassVertexBuffer = null;

// Model-view and projection matrix and model-view matrix stack
var mvMatrixStack = [];
var mvMatrix = mat4.create();
var pMatrix = mat4.create();

// Variables for storing textures
var wallTexture;

// Variable that stores  loading state of textures.
var texturesLoaded = false;
var dataLoaded = false;

// Keyboard handling helper variable for reading the status of keys
var currentlyPressedKeys = {};

// Variables for storing current position and speed
var pitch = 0;
var pitchRate = 0;
var yaw = 0;
var yawRate = 0;
var xPosition = 2.5;
var yPosition = 0.4;
var zPosition = -3;
var speed = 0;

// Used to make us "jog" up and down as we move forward.
var joggingAngle = 0;

// Helper variable for animation
var lastTime = 0;

// desni klik za scope
var rightDown = false;

// game stuff variables

// models
var monsterModel;
var snipeModel;
var grassModel;
var wallModel;

var grassModel_1;

var towerModel;

var skyboxModel;

var dragonModel_1;

var scopeModel;

// gameObjects
var snipe_1;

var scope;

var grass_1;

var wall_1;

var grassM_1;

var skybox_1;

var dragon_1;


// lista kjer so vse trave
var grassPositionsX = [];
var grassPositionsY = [];

// lista vseh zmajev
var dragonList = [];
var dragonAngle = 0;

// monsters manager
var monsterManager;




// start_______________________________________________________
// klican ko je kreiran canvas
function start() {
  canvas = document.getElementById("glcanvas");

  canvas.addEventListener('mousedown', function(evt) {
    mouseDown(evt);  
  });

  canvas.addEventListener('mouseup', function(evt) {
    mouseUp(evt);  
  });

  gl = initGL(canvas);      // Initialize the GL context

  // Only continue if WebGL is available and working
  if (gl) {
    gl.clearColor(0.0, 0.0, 0.0, 1.0);                      // Set clear color to black, fully opaque
    gl.clearDepth(1.0);                                     // Clear everything
    gl.enable(gl.DEPTH_TEST);                               // Enable depth testing
    gl.depthFunc(gl.LEQUAL);                                // Near things obscure far things

    // preberem file
    //readFile();

    initShaders();
    initTextures();
    initModels();


    snipe_1 = new gameObject(snipeModel, [0,0,0]);

    grass_1 = new gameObject(grassModel, [0,0,0]);

    wall_1 = new gameObject(wallModel, [0,0,0]);

    grassM_1 = new gameObject(grassModel_1, [0,0,0]);

    dragon_1 = new gameObject(dragonModel_1, [0,0,0]);

    scope = new gameObject(scopeModel, [0,0,0]);

    // leteci zmaji
    for(var i = 0; i < 300; i+= 8){
      var d = new gameObject(dragonModel_1, [0,30 + i,0]);
      d.angle = Math.random() * 360;
      d.radius = 40 + Math.random() * 100;
      d.speed = 0.06 + Math.random() * 0.4;
      dragonList.push(d);
    }
    

    for(var i = 0; i < 600; i++){
      var gx = Math.random() *  200;
      var gz = Math.random() * -400;

      grassPositionsX.push(gx);
      grassPositionsY.push(gz);
    }

    // posasti
    monsterManager = new monsters();


    // Bind keyboard handling functions to document handlers
    document.onkeydown = handleKeyDown;
    document.onkeyup = handleKeyUp;

    document.onclick = mouseClick;

    // pointer lock
    pointerLockSetup();
    
    // Set up to draw the scene periodically.
    setInterval(function() {
      if (texturesLoaded) { // only draw scene and animate when textures are loaded.
        handleKeys();
        requestAnimationFrame(animate);
        drawScene();
      }
    }, 15);
  }


}

//
// Matrix utility functions
//
// mvPush   ... push current matrix on matrix stack
// mvPop    ... pop top matrix from stack
// degToRad ... convert degrees to radians
//
function mvPushMatrix() {
  var copy = mat4.create();
  mat4.set(mvMatrix, copy);
  mvMatrixStack.push(copy);
}

function mvPopMatrix() {
  if (mvMatrixStack.length == 0) {
    throw "Invalid popMatrix!";
  }
  mvMatrix = mvMatrixStack.pop();
}

function degToRad(degrees) {
  return degrees * Math.PI / 180;
}

//
// initGL
//
// Initialize WebGL, returning the GL context or null if
// WebGL isn't available or could not be initialized.
//
function initGL(canvas) {
  var gl = null;
  try {
    // Try to grab the standard context. If it fails, fallback to experimental.
    gl = canvas.getContext("webgl") || canvas.getContext("experimental-webgl");
    gl.viewportWidth = canvas.width;
    gl.viewportHeight = canvas.height;
  } catch(e) {}

  // If we don't have a GL context, give up now
  if (!gl) {
    alert("Unable to initialize WebGL. Your browser may not support it.");
  }
  return gl;
}

//
// getShader
//
// Loads a shader program by scouring the current document,
// looking for a script with the specified ID.
//
function getShader(gl, id) {
  var shaderScript = document.getElementById(id);

  // Didn't find an element with the specified ID; abort.
  if (!shaderScript) {
    return null;
  }
  
  // Walk through the source element's children, building the
  // shader source string.
  var shaderSource = "";
  var currentChild = shaderScript.firstChild;
  while (currentChild) {
    if (currentChild.nodeType == 3) {
        shaderSource += currentChild.textContent;
    }
    currentChild = currentChild.nextSibling;
  }
  
  // Now figure out what type of shader script we have,
  // based on its MIME type.
  var shader;
  if (shaderScript.type == "x-shader/x-fragment") {
    shader = gl.createShader(gl.FRAGMENT_SHADER);
  } else if (shaderScript.type == "x-shader/x-vertex") {
    shader = gl.createShader(gl.VERTEX_SHADER);
  } else {
    return null;  // Unknown shader type
  }

  // Send the source to the shader object
  gl.shaderSource(shader, shaderSource);

  // Compile the shader program
  gl.compileShader(shader);

  // See if it compiled successfully
  if (!gl.getShaderParameter(shader, gl.COMPILE_STATUS)) {
    alert(gl.getShaderInfoLog(shader));
    return null;
  }

  return shader;
}

//
// initShaders
//
// Initialize the shaders, so WebGL knows how to light our scene.
//
function initShaders() {
  var fragmentShader = getShader(gl, "shader-fs");
  var vertexShader = getShader(gl, "shader-vs");
  
  // Create the shader program
  shaderProgram = gl.createProgram();
  gl.attachShader(shaderProgram, vertexShader);
  gl.attachShader(shaderProgram, fragmentShader);
  gl.linkProgram(shaderProgram);
  
  // If creating the shader program failed, alert
  if (!gl.getProgramParameter(shaderProgram, gl.LINK_STATUS)) {
    alert("Unable to initialize the shader program.");
  }
  
  // start using shading program for rendering
  gl.useProgram(shaderProgram);
  
  // store location of aVertexPosition variable defined in shader
  shaderProgram.vertexPositionAttribute = gl.getAttribLocation(shaderProgram, "aVertexPosition");

  // turn on vertex position attribute at specified position
  gl.enableVertexAttribArray(shaderProgram.vertexPositionAttribute);

  // store location of aVertexNormal variable defined in shader
  shaderProgram.textureCoordAttribute = gl.getAttribLocation(shaderProgram, "aTextureCoord");

  // store location of aTextureCoord variable defined in shader
  gl.enableVertexAttribArray(shaderProgram.textureCoordAttribute);

  // store location of uPMatrix variable defined in shader - projection matrix 
  shaderProgram.pMatrixUniform = gl.getUniformLocation(shaderProgram, "uPMatrix");
  // store location of uMVMatrix variable defined in shader - model-view matrix 
  shaderProgram.mvMatrixUniform = gl.getUniformLocation(shaderProgram, "uMVMatrix");
  // store location of uSampler variable defined in shader
  shaderProgram.samplerUniform = gl.getUniformLocation(shaderProgram, "uSampler");
}

//
// setMatrixUniforms
//
// Set the uniforms in shaders.
//
function setMatrixUniforms() {
  gl.uniformMatrix4fv(shaderProgram.pMatrixUniform, false, pMatrix);
  gl.uniformMatrix4fv(shaderProgram.mvMatrixUniform, false, mvMatrix);
}

//
// initTextures
//
// Initialize the textures we'll be using, then initiate a load of
// the texture images. The handleTextureLoaded() callback will finish
// the job; it gets called each time a texture finishes loading.
//
function initTextures() {
  wallTexture = gl.createTexture();
  wallTexture.image = new Image();
  wallTexture.image.onload = function () {
    handleTextureLoaded(wallTexture)
  }
  wallTexture.image.src = "./assets/wall.png";
}

function handleTextureLoaded(texture) {
  gl.pixelStorei(gl.UNPACK_FLIP_Y_WEBGL, true);

  // Third texture usus Linear interpolation approximation with nearest Mipmap selection
  gl.bindTexture(gl.TEXTURE_2D, texture);
  gl.texImage2D(gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, texture.image);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.LINEAR);
  gl.texParameteri(gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.LINEAR);
  gl.generateMipmap(gl.TEXTURE_2D);

  gl.bindTexture(gl.TEXTURE_2D, null);

  // when texture loading is finished we can draw scene.
  texturesLoaded = true;
}


function initModels(){
  monsterModel = new JSONmodel("./assets/monster.JSON", "./assets/monster.jpg");

  snipeModel = new JSONmodel("./assets/Snipe.JSON", "./assets/Snipe.BMP");

  grassModel = new model("./assets/Grass.txt", "./assets/grass.png");
  wallModel = new model("./assets/wall.txt", "./assets/rock-wall_2.png");

  grassModel_1 = new JSONmodel("./assets/grassModel_1.JSON", "./assets/grassModel_1.jpg");

  towerModel = new JSONmodel("./assets/tower.JSON", "./assets/tower.jpg");

  skyboxModel = new model("./assets/skybox.txt", "./assets/skybox.jpg", true);

  dragonModel_1 = new JSONmodel("./assets/dragon.json", "./assets/dragon.jpg", true);

  scopeModel = new model("./assets/scope.txt", "./assets/scope.png", true);



}


//
// handleLoadedWorld
//
// Initialisation of world 
//
function handleLoadedWorld(data) {
  var lines = data.split("\n");
  var vertexCount = 0;
  var vertexPositions = [];
  var vertexTextureCoords = [];
  for (var i in lines) {
    var vals = lines[i].replace(/^\s+/, "").split(/\s+/);
    if (vals.length == 5 && vals[0] != "//") {
      // It is a line describing a vertex; get X, Y and Z first
      vertexPositions.push(parseFloat(vals[0]));
      vertexPositions.push(parseFloat(vals[1]));
      vertexPositions.push(parseFloat(vals[2]));

      // And then the texture coords
      vertexTextureCoords.push(parseFloat(vals[3]));
      vertexTextureCoords.push(parseFloat(vals[4]));

      vertexCount += 1;
    }
  }

  worldVertexPositionBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, worldVertexPositionBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertexPositions), gl.STATIC_DRAW);
  worldVertexPositionBuffer.itemSize = 3;
  worldVertexPositionBuffer.numItems = vertexCount;

  worldVertexTextureCoordBuffer = gl.createBuffer();
  gl.bindBuffer(gl.ARRAY_BUFFER, worldVertexTextureCoordBuffer);
  gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(vertexTextureCoords), gl.STATIC_DRAW);
  worldVertexTextureCoordBuffer.itemSize = 2;
  worldVertexTextureCoordBuffer.numItems = vertexCount;

  document.getElementById("loadingtext").textContent = "";
}

//
// loadWorld
//
// Loading world 
//
function loadWorld() {
  var request = new XMLHttpRequest();
  request.open("GET", "./assets/world.txt");
  request.onreadystatechange = function () {
    if (request.readyState == 4) {
      handleLoadedWorld(request.responseText);
    }
  }
  request.send();
}


var timeStack = [];
// izracuna fps in jih prikaze
// vzame 10 casov med slicicami in izracuna povprecje
function calculateFPS(time){
  timeStack.push(time);
  if(timeStack.length >= 10){
    var sum = 0;
    for(var i = 0; i < timeStack.length; i++) sum += timeStack[i];
    //console.log(5000 / sum);
    document.getElementById("FPS").innerText = "FPS: " + Math.floor(10000 / sum);
  timeStack = [];
  }
}

// za izracun FPS
var zadnjiCas = new Date().getTime();

// drawScene
function drawScene() {
  calculateFPS(new Date().getTime() - zadnjiCas);
  zadnjiCas = new Date().getTime();
  // set the rendering environment to full canvas size
  gl.viewport(0, 0, gl.viewportWidth, gl.viewportHeight);
  // Clear the canvas before we start drawing on it.
  gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);


  
  // Establish the perspective with which we want to view the
  // scene. Our field of view is 45 degrees, with a width/height
  // ratio of 640:480, and we only want to see objects between 0.1 units
  // and 100 units away from the camera.
  mat4.perspective(45, gl.viewportWidth / gl.viewportHeight, 0.1, 10000.0, pMatrix);

  // Set the drawing position to the "identity" point, which is
  // the center of the scene.
  mat4.identity(mvMatrix);

  // Now move the drawing position a bit to where we want to start
  // drawing the world.
  mat4.rotate(mvMatrix, degToRad(-pitch), [1, 0, 0]);
  mat4.rotate(mvMatrix, degToRad(-yaw), [0, 1, 0]);
  mat4.translate(mvMatrix, [-xPosition, -yPosition, -zPosition]);


  
  

  if(rightDown) scope.draw();
  else snipe_1.draw();

  grass_1.draw();

  wall_1.draw();
  //grassM_1.draw();

  mvPushMatrix();
    mat4.scale(mvMatrix, [0.1, 0.1, 0.1]);
    mat4.translate(mvMatrix, [0, 0.5, 0]);

    mat4.translate(mvMatrix, [0, 0, -10]);

    for(var i = 0; i < grassPositionsX.length; i++){
        mat4.translate(mvMatrix, [grassPositionsX[i], 0, grassPositionsY[i]]);
        grassM_1.draw();
        mat4.translate(mvMatrix, [-grassPositionsX[i], 0, -grassPositionsY[i]]);
    }
    
  
  mvPopMatrix();

  mvPushMatrix();
  mat4.scale(mvMatrix, [10,10,10]);
  mat4.translate(mvMatrix, [-0.25,1,-0.25]);
  for(var i = 0; i < 4; i++){
    towerModel.draw();
    mat4.translate(mvMatrix, [2.6,0,0]);
    towerModel.draw();
    mat4.translate(mvMatrix, [-2.6,0,0]);

    mat4.translate(mvMatrix, [0,0,-1.2])
  }

  mvPopMatrix();

  mvPushMatrix();
  mat4.translate(mvMatrix, [-1000, -1000, 1000]);
  mat4.scale(mvMatrix, [2000,2000,2000]);
  skyboxModel.draw();
  mvPopMatrix();

  mvPushMatrix();

  mat4.translate(mvMatrix, [26,20,-26]);
  mat4.scale(mvMatrix, [10,10,10]);
  dragon_1.draw();
  mvPopMatrix();

  for(var i = 0; i < dragonList.length; i++){
    dragonList[i].draw();
  }

  monsterManager.draw();

  

}

//
// animate
//
// Called every time before redeawing the screen.
//
function animate() {

  var timeNow = new Date().getTime();
  if (lastTime != 0) {
    var elapsed = timeNow - lastTime;

    if (speed != 0) {
      // izracuna nov x in z
      var newX = xPosition - Math.sin(degToRad(yaw)) * speed * elapsed;
      var newZ = zPosition - Math.cos(degToRad(yaw)) * speed * elapsed;
      
      // preveri ali je prislo do kolizije
      if(newX > 1 && newX < 19 && newZ < -1 && newZ > -39){
        xPosition = newX;
        zPosition = newZ;
        joggingAngle += elapsed * 0.6; // 0.6 "fiddle factor" - makes it feel more realistic :-)
        yPosition = Math.sin(degToRad(joggingAngle)) / 20 + 0.4
      }

      

    }
    //console.log(speed);

    yaw += yawRate * elapsed;
    // preveri ali je kot 
    if(pitch + pitchRate * elapsed > -16 && pitch + pitchRate * elapsed < 65){
      pitch += pitchRate * elapsed;
    }
    

    /*
    monster_1.position = [monster_1.position[0] += 0.003, 0, 0];
    cube_1.position = [cube_1.position[0] += 0.003, 0, 0];
    */

    var dist = 0.35;
    snipe_1.position = [(xPosition + Math.cos(-degToRad(yaw + 80)) * dist)  , 
                        yPosition +  Math.sin(degToRad(pitch)) * dist - 0.2, 
                        (zPosition + Math.sin(-degToRad(yaw + 80)) * dist)];
    snipe_1.scale = [0.5,0.5,0.5];
    snipe_1.Yrotation = yaw + 180;
    snipe_1.Xrotation = -pitch;

    scope.position = [(xPosition + Math.cos(-degToRad(yaw + 90)) * dist)  , 
                        yPosition +  Math.sin(degToRad(pitch)) * dist + 0.04, 
                        (zPosition + Math.sin(-degToRad(yaw + 90)) * dist)];
    scope.scale = [0.2,0.2,0.2];
    scope.Yrotation = yaw + 180;
    scope.Xrotation = -pitch;





    for(var i = 0; i < dragonList.length; i++){
      dragonList[i].position = [10 + Math.cos(degToRad(dragonList[i].angle)) * dragonList[i].radius,
                            dragonList[i].position[1],
                            -100 + Math.sin(degToRad(dragonList[i].angle)) * dragonList[i].radius
                            ];
      dragonList[i].Yrotation = -dragonList[i].angle +80;
      dragonList[i].angle += dragonList[i].speed;
      dragonList[i].scale = [10,10,10];
    }

    monsterManager.animate(elapsed);

    updateStats();



  }
  lastTime = timeNow;
}

// Posodobi vse podatke ki so prikazani na zaslon
function updateStats(){
  // pozicija kamere
  document.getElementById("X").innerText = "X: " + xPosition.toFixed(2);
  document.getElementById("Y").innerText = "Y: " + yPosition.toFixed(2);
  document.getElementById("Z").innerText = "Z: " + zPosition.toFixed(2);
  // kot kamere
  document.getElementById("yaw").innerText =   "yaw:   " + yaw.toFixed(2);
  document.getElementById("pitch").innerText = "pitch: " + pitch.toFixed(2);
}


// Keyboard handling helper functions________________________________________________
//
// handleKeyDown    ... called on keyDown event
// handleKeyUp      ... called on keyUp event
//
function handleKeyDown(event) {
  // storing the pressed state for individual key
  currentlyPressedKeys[event.keyCode] = true;
  //console.log("KeyDown " + event.keyCode);
}

function handleKeyUp(event) {
  // reseting the pressed state for individual key
  currentlyPressedKeys[event.keyCode] = false;
  //console.log("KeyUp " + event.keyCode);
}

//
// handleKeys
//
// Called every time before redeawing the screen for keyboard
// input handling. Function continuisly updates helper variables.
//
function handleKeys() {
  if (currentlyPressedKeys[33]) {
    // Page Up
    pitchRate = 0.1;
  } else if (currentlyPressedKeys[34]) {
    // Page Down
    pitchRate = -0.1;
  } else {
    pitchRate = 0;
  }

  if (currentlyPressedKeys[37] || currentlyPressedKeys[65]) {
    // Left cursor key or A
    yawRate = 0.1;
    //console.log("Pritisk A");
  } else if (currentlyPressedKeys[39] || currentlyPressedKeys[68]) {
    // Right cursor key or D
    yawRate = -0.1;
  } else {
    yawRate = 0;
  }


  if (currentlyPressedKeys[38] || currentlyPressedKeys[87]) {
    if(currentlyPressedKeys[16]) speed = 0.006;
    else speed = 0.003;
    
  } else if (currentlyPressedKeys[40] || currentlyPressedKeys[83]) {
      if(currentlyPressedKeys[16]) speed = -0.006;
      else speed = -0.003;
  } else {
    speed = 0;
  }
}



// pointer lock and mouse events _______________________________________________________
function pointerLockSetup(){

  canvas.onclick = 
    function() {
      canvas.requestPointerLock();
    };

  // pointer lock event listeners

  // Hook pointer lock state change events for different browsers
  document.addEventListener('pointerlockchange', lockChangeAlert, false);
  document.addEventListener('mozpointerlockchange', lockChangeAlert, false);
  document.addEventListener('webkitpointerlockchange', lockChangeAlert, false);
}


function lockChangeAlert() {
  if(document.pointerLockElement === canvas ||
  document.mozPointerLockElement === canvas ||
  document.webkitPointerLockElement === canvas) {
    document.addEventListener("mousemove", mousemove, false);
  } else {
    document.removeEventListener("mousemove", mousemove, false);
  }
}


// event vrne premike miske v pikslih
// aktivira se le ko je mouse pointer locked
function mousemove(e) {

  // pridobi premim po x smeri v pikslih
  var movementX = e.movementX ||
      e.mozMovementX          ||
      e.webkitMovementX       ||
      0;

  // pridobi premim po y smeri v pikslih
  var movementY = e.movementY ||
      e.mozMovementY      ||
      e.webkitMovementY   ||
      0;

  pitchRate = 0.1 * (-movementY / 20);
  yawRate =   0.1 * (-movementX / 20);
  
}

function mouseClick(){
  monsterManager.shot();
}

function mouseDown(evt){
  // right click
  if(evt.buttons == 2){
    rightDown = true;
  }
}

function mouseUp(evt){
  // right click
  if(evt.which == 3){
    rightDown = false;
  }

}