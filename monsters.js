class monsters{
	constructor(){
		this.monstersList = [];
		this.monstersSpeed = 0.002;

		// spawn x pozicije posasti
		this.xList = [4, 8, 12, 16];

		this.distanceMeter = 0;
		
	}

	// kreira posasti glede na level
	createMonster(){
		// preveri ali lahko kreira novo posast
		if(this.distanceMeter < 5) return;
		var xPos = Math.round(Math.random() * this.xList.length);
		this.monstersList.push(new gameObject(monsterModel, 
								[this.xList[xPos],
								 0,
								-38
		 	]));

		this.distanceMeter = 0;

	}

	// postavi posasti na pravo mesto in jih premakne
	animate(elapsed){
		// preveri ali si izguubil igro
		if(this.lost()){
			alert("Izgubil si!");
			location.reload();
		}
		// preveri ali je cas za kreiranje nove posasti
		this.createMonster();
		// premakne vse posasti za eno mesto naprej
		for(var i = 0; i < this.monstersList.length; i++){
			this.monstersList[i].position = [
				this.monstersList[i].position[0],
				this.monstersList[i].position[1],
				this.monstersList[i].position[2] += this.monstersSpeed * elapsed
				]
		}

		this.distanceMeter += this.monstersSpeed * elapsed;
	}

	// preveri ali je bila katera posast zadeta in jo izbrise
	shot(){
		// dobi pozicijo kamere
		var camPos = [xPosition, yPosition, zPosition];
		// dobi smerni vektor
		var smerniVektor = [(xPosition + Math.cos(-degToRad(yaw + 90)))  , 
                        yPosition +  Math.sin(degToRad(pitch)), 
                        (zPosition + Math.sin(-degToRad(yaw + 90)))];

        var b = [smerniVektor[0] - camPos[0],
        					  smerniVektor[1] - camPos[1],
        					  smerniVektor[2] - camPos[2]];

        var tempArray = [];
  		// grem skozi posasti in vidim ce je bila kaka zadeta
  		for(var i = 0; i < this.monstersList.length; i++){
  			// dobim pozicijo posasti
  			var monsterPosition = [this.monstersList[i].position[0],
  								   this.monstersList[i].position[1] + 0.7,
  								   this.monstersList[i].position[2]
  								  ];

  			// vektor med kamero in posastjo
  			var a = [monsterPosition[0] - camPos[0],
  								 monsterPosition[1] - camPos[1],
  								 monsterPosition[2] - camPos[2]];
		 	// pravokotna projekcija monsterVector na gunPointVector
		 	var ratio = (a[0]*b[0] + a[1]*b[1] + a[2]*b[2]) / (vec3.length(b) * vec3.length(b));

		 	// vektor po projekciji
		 	var c = [b[0] * ratio, b[1] * ratio, b[2] * ratio];

		 	var razdalja = vec3.length([c[0] - a[0], c[1] - a[1], c[2] - a[2]]);

		 	if(razdalja > 0.3) tempArray.push(this.monstersList[i]); 
  		}

  		this.monstersList = tempArray;
	}

	// vrne true ce je katera posast prisla na drugo stran
	lost(){
		if(this.monstersList.length == 0) return false;
		for(var i = 0; i < this.monstersList.length; i++){
			if(this.monstersList[i].position[2] > -1) return true;
		}
		return false;
	}

	// izris vseh posasti
	draw(){
		for(var i = 0; i < this.monstersList.length; i++){
			this.monstersList[i].draw();
		}
	}
}